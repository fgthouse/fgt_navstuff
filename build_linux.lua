local SDK_DIR = "/home/unknown/Development/source-sdk-2013/" -- Change this to your source-sdk-2013 github location

solution "gmsv_fgt_navstuff"

	language "C++"
	platforms { "x32" }
	location ( os.get() .."-".. _ACTION )
	flags { "Symbols", "NoEditAndContinue", "NoPCH", "StaticRuntime", "EnableSSE" }
	targetdir ( "lib/" .. os.get() .. "/" )
	includedirs {	"../../GmodModules/gmod-module-base/include",
					SDK_DIR .. "mp/src/public",
					SDK_DIR .. "mp/src/public/tier0",
					SDK_DIR .. "mp/src/public/tier1",
					SDK_DIR .. "mp/src/game/server",
					SDK_DIR .. "mp/src/game/shared",
				}
	libdirs { SDK_DIR .. "mp/src/lib/public/linux32" }
	
	configurations { "Release" }
		
	configuration "Release"
		defines { "NDEBUG" }
		flags{ "Optimize", "FloatFast" }
	
	project "gmsv_fgt_navstuff_linux"
		defines { "POSIX", "LINUX", "_LINUX", "GNUC", "GMMODULE", "GAME_DLL" }
		buildoptions { "-Wno-invalid-offsetof" }-- I'm not sure how to fix these warnings... (I've been able to ignore them safely though.)
		links { "vstdlib", "tier0" }
		linkoptions { -- Static links. For some reason ldd -d reports this as missing but once it's loaded it finds them...
			SDK_DIR .. "mp/src/lib/public/linux32/mathlib.a",
			SDK_DIR .. "mp/src/lib/public/linux32/tier2.a",
			SDK_DIR .. "mp/src/lib/public/linux32/tier1.a",
		}
		files { "src/**.*", "../../GmodModules/gmod-module-base/include/**.*" }
		kind "SharedLib"
		
