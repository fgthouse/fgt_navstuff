fgt_navstuff
=================

This module is now deprecated as of 4/24/2015

All of the functions that this module added in the past are now implemented into garrysmod by default.

The PlayerEntered/ExitedNavArea hooks are not implemented, but can easily be added through lua with the new bindings.
