#include "GarrysMod/Lua/Interface.h"

using namespace GarrysMod::Lua;
extern int CNavAreaM_RefID;
extern int VectorMeta_RefID;

int CNavArea_IsValid( lua_State *state ){
	LUA->CheckType( 1, 37 );

	UserData *ud = (UserData*)LUA->GetUserdata( 1 );
	CNavArea *navArea = (CNavArea*)ud->data;
	if( !navArea || !navArea->GetID() ){ // Invalid CNavArea
		LUA->PushBool( false );
	} else {
		LUA->PushBool( true );
	}
	return 1;
}

int CNavArea_GetAttributes( lua_State *state ){
	LUA->CheckType( 1, 37 );

	UserData *ud = (UserData*)LUA->GetUserdata( 1 );
	CNavArea *navArea = (CNavArea*)ud->data;
	if( !navArea ){ // Invalid CNavArea. (Make sure you always check if its valid. Otherwise you crash.)
		LUA->PushNil();
		return 1; // Return nil if we're given an invalid CNavArea.
	}

	LUA->PushNumber( navArea->GetAttributes() );
	return 1;
}

// int GetAdjacentCount( NavDirType dir ) const    { return m_connect[ dir ].Count(); }    // return number of connected areas in given direction
int CNavArea_GetAdjacentCount( lua_State *state ){
	LUA->CheckType( 1, 37 ); // 37 is Type::CNAVAREA (if it existed)
	LUA->CheckType( 2, Type::NUMBER );

	UserData *ud = (UserData*)LUA->GetUserdata( 1 );
	CNavArea *navArea = (CNavArea*)ud->data;
	if( !navArea ){ // Invalid CNavArea. (Make sure you always check if its valid. Otherwise you crash.)
		LUA->PushNil();
		return 1; // Return nil if we're given an invalid CNavArea.
	}
	int dir = LUA->GetNumber( 2 );

	LUA->PushNumber( navArea->GetAdjacentCount( (NavDirType)dir ) );

	return 1;
}

// CNavArea *GetAdjacentArea( NavDirType dir, int i ) const;       // return the i'th adjacent area in the given direction
int CNavArea_GetAdjacentArea( lua_State *state ){
	LUA->CheckType( 1, 37 );
	LUA->CheckType( 2, Type::NUMBER );
	LUA->CheckType( 3, Type::NUMBER );

	UserData *ud = (UserData*)LUA->GetUserdata( 1 );
	CNavArea *navArea = (CNavArea*)ud->data;
	if( !navArea ){ // Invalid CNavArea. (Make sure you always check if its valid. Otherwise you crash.)
		LUA->PushNil();
		return 1; // Return nil if we're given an invalid CNavArea.
	}
	int dir = LUA->GetNumber( 2 );
	int i = LUA->GetNumber( 3 );

	CNavArea *ret = navArea->GetAdjacentArea( (NavDirType)dir, i );
	if( ret ){
		UserData *retUd = (UserData*)LUA->NewUserdata( sizeof( UserData ) );
		retUd->data = ret;
		retUd->type = 37;
		LUA->ReferencePush( CNavAreaM_RefID );
		LUA->SetMetaTable( -2 ); // Set _R.CNavArea as the metatable for our userdata.
	} else {
		LUA->PushNil();
	}

	return 1;
}

// CNavArea *GetRandomAdjacentArea( NavDirType dir ) const;
int CNavArea_GetRandomAdjacentArea( lua_State *state ){
	LUA->CheckType( 1, 37 );
	LUA->CheckType( 2, Type::NUMBER );

	UserData *ud = (UserData*)LUA->GetUserdata( 1 );
	FgtNavArea *navArea = (FgtNavArea*)ud->data;
	if( !navArea ){ // Invalid CNavArea. (Make sure you always check if its valid. Otherwise you crash.)
		LUA->PushNil();
		return 1; // Return nil if we're given an invalid CNavArea.
	}
	int dir = LUA->GetNumber( 2 );

	CNavArea *ret = navArea->GetRandomAdjacentArea( (NavDirType)dir );
	if( ret ){
		UserData *retUd = (UserData*)LUA->NewUserdata( sizeof( UserData ) );
		retUd->data = ret;
		retUd->type = 37;
		LUA->ReferencePush( CNavAreaM_RefID );
		LUA->SetMetaTable( -2 ); // Set _R.CNavArea as the metatable for our userdata.`
	} else {
		LUA->PushNil();
	}

	return 1;
}

// This function is heavy for some reason. (2-4ms) Find out why...
int CNavArea_CollectAdjacentAreas( lua_State *state ){
	LUA->CheckType( 1, 37 );
	UserData *ud = (UserData*)LUA->GetUserdata( 1 );
	FgtNavArea *navArea = (FgtNavArea*)ud->data;

	if( !navArea ){ // Invalid CNavArea. (Make sure you always check if its valid. Otherwise you crash.)
		LUA->PushNil();
		return 1; // Return nil if we're given an invalid CNavArea.
	}
	std::vector< CNavArea* > adjVector;
	navArea->CollectAdjacentAreas( &adjVector );

	LUA->CreateTable();

	for( unsigned int i = 1; i <= adjVector.size(); i++ ){
		LUA->PushNumber( i ); // K
			
		CNavArea *nav = adjVector[ i - 1 ]; // V
		UserData *retUd = (UserData*)LUA->NewUserdata( sizeof( UserData ) );
		retUd->data = nav;
		retUd->type = 37;
		LUA->ReferencePush( CNavAreaM_RefID );
		LUA->SetMetaTable( -2 );

		LUA->SetTable( -3 ); // table[ i ] = nav
	}

	return 1; // This will return an empty table if there are no adjacent areas
}

int CNavArea_IsConnected( lua_State *state ){
	LUA->CheckType( 1, 37 );
	LUA->CheckType( 2, 37 );
	LUA->CheckType( 3, Type::NUMBER );

	UserData *ud = (UserData*)LUA->GetUserdata( 1 );
	FgtNavArea *navArea = (FgtNavArea*)ud->data;
	if( !navArea ){ // Invalid CNavArea. (Make sure you always check if its valid. Otherwise you crash.)
		LUA->PushNil();
		return 1; // Return nil if we're given an invalid CNavArea.
	}

	UserData *ud2 = (UserData*)LUA->GetUserdata( 2 );
	CNavArea *givenArea = (CNavArea*)ud2->data;
	int dir = LUA->GetNumber( 3 ); // If dir == NUM_DIRECTIONS (Size of NavDirType = 4) check all sides.
	bool ret = navArea->IsConnected( givenArea, (NavDirType)dir );
	LUA->PushBool( ret );

	return 1;
}

int CNavArea_GetPlayerCount( lua_State *state ){
	LUA->CheckType( 1, 37 );
	LUA->CheckType( 2, Type::NUMBER );

	UserData *ud = (UserData*)LUA->GetUserdata( 1 );
	CNavArea *navArea = (CNavArea*)ud->data;
	if( !navArea ){ // Invalid CNavArea. (Make sure you always check if its valid. Otherwise you crash.)
		LUA->PushNil();
		return 1; // Return nil if we're given an invalid CNavArea.
	}

	int teamId = LUA->GetNumber( 2 );
	LUA->PushNumber( navArea->GetPlayerCount( teamId ) );
	return 1;
}

int CNavArea_ComputeGroundHeightChange( lua_State *state ){
	LUA->CheckType( 1, 37 );
	LUA->CheckType( 2, 37 );

	UserData *ud = (UserData*)LUA->GetUserdata( 1 );
	FgtNavArea *navArea = (FgtNavArea*)ud->data;
	if( !navArea ){
		LUA->PushNil();
		return 1;
	}

	UserData *ud2 = (UserData*)LUA->GetUserdata( 2 );
	FgtNavArea *navArea2 = (FgtNavArea*)ud2->data;
	if( !navArea2 ){
		LUA->PushNil();
		return 1;
	}

	LUA->PushNumber( navArea->ComputeGroundHeightChange( navArea2 ) );
	return 1;
}

int CNavArea_GetCenter( lua_State *state ){
	LUA->CheckType( 1, 37 );

	UserData *ud = (UserData*)LUA->GetUserdata( 1 );
	CNavArea *navArea = (CNavArea*)ud->data;
	if( !navArea ){
		LUA->PushNil();
		return 1;
	}

	Vector vec = navArea->GetCenter();
	if( vec.IsValid() ){
		UserData *retUd = (UserData*)LUA->NewUserdata( sizeof( UserData ) );
		retUd->data = &vec;
		retUd->type = GarrysMod::Lua::Type::VECTOR;
		LUA->ReferencePush( VectorMeta_RefID );
		LUA->SetMetaTable( -2 ); // Set _R.CNavArea as the metatable for our userdata.`

	} else {
		LUA->PushNil();
	}

	return 1;
}

int CNavArea_ComputeDirection( lua_State *state ){
	LUA->CheckType( 1, 37 );
	LUA->CheckType( 2, Type::VECTOR );

	UserData *ud = (UserData*)LUA->GetUserdata( 1 );
	FgtNavArea *navArea = (FgtNavArea*)ud->data;
	if( !navArea ){
		LUA->PushNil();
		return 1;
	}

	UserData *ud2 = (UserData*)LUA->GetUserdata( 2 );
	Vector *vec = (Vector*)ud2->data;
	if( !vec ){
		LUA->PushNil();
		return 1;
	}

	LUA->PushNumber( navArea->ComputeDirection( vec ) );
	return 1;
}

int CNavArea_IsOverlappingArea( lua_State *state ){
	LUA->CheckType( 1, 37 );
	LUA->CheckType( 2, 37 );

	UserData *ud = (UserData*)LUA->GetUserdata( 1 );
	FgtNavArea *navArea = (FgtNavArea*)ud->data;
	if( !navArea ){
		LUA->PushNil();
		return 1;
	}

	UserData *ud2 = (UserData*)LUA->GetUserdata( 2 );
	FgtNavArea *navArea2 = (FgtNavArea*)ud2->data;
	if( !navArea2 ){
		LUA->PushNil();
		return 1;
	}

	LUA->PushBool( navArea->IsOverlappingArea( navArea2 ) );
	return 1;
}

int CNavArea_IsBlocked( lua_State *state ){
	LUA->CheckType( 1, 37 ); // self cnavarea
	LUA->CheckType( 2, Type::NUMBER ); // team
	if( LUA->Top() >= 3 ){ // ignoreNavBlockers
		LUA->CheckType( 3, Type::BOOL );
	}

	UserData *ud = (UserData*)LUA->GetUserdata( 1 );
	FgtNavArea *navArea = (FgtNavArea*)ud->data;
	if( !navArea ){
		LUA->PushNil();
		return 1;
	}

	bool ret;
	if( LUA->Top() >= 3 ){
		ret = navArea->IsBlocked( LUA->GetNumber( 2 ), LUA->GetBool( 3 ) );
	} else {
		ret = navArea->IsBlocked( LUA->GetNumber( 2 ) );
	}

	LUA->PushBool( ret );
	return 1;
}

int CNavArea_MarkAsBlocked( lua_State *state ){
	LUA->CheckType( 1, 37 ); // cnavarea
	LUA->CheckType( 2, Type::NUMBER ); // team
	LUA->CheckType( 3, Type::ENTITY ); // blocking ent
	if( LUA->Top() >= 4 ){ // generateevent
		LUA->CheckType( 4, Type::BOOL );
	}

	UserData *ud = (UserData*)LUA->GetUserdata( 1 );
	FgtNavArea *navArea = (FgtNavArea*)ud->data;
	if( !navArea ){
		LUA->PushNil();
		return 1;
	}

	UserData *entUd = (UserData*)LUA->GetUserdata( 3 );
	if( !entUd || !entUd->data ){ // Invalid entity
		LUA->PushNil();
		return 1;
	}

	CBaseHandle entHandle = *(CBaseHandle*)entUd->data;
	if( !entHandle.IsValid() ){ // Invalid handle
		LUA->PushNil();
		return 1;
	}

	CBaseEntity *ent = engine->PEntityOfEntIndex( entHandle.GetEntryIndex() )->GetUnknown()->GetBaseEntity();
	if( LUA->Top() >= 4 ){
		navArea->MarkAsBlocked( LUA->GetNumber( 2 ), ent, LUA->GetBool( 4 ) );
	} else {
		navArea->MarkAsBlocked( LUA->GetNumber( 2 ), ent );
	}

	LUA->PushBool( true );
	return 1;
}

int CNavArea_UnblockArea( lua_State *state ){
	LUA->CheckType( 1, 37 ); //cnavarea
	if( LUA->Top() >= 2 ){
		LUA->CheckType( 2, Type::NUMBER ); // team
	}

	UserData *ud = (UserData*)LUA->GetUserdata( 1 );
	FgtNavArea *navArea = (FgtNavArea*)ud->data;
	if( !navArea ){
		LUA->PushNil();
		return 1;
	}

	if( LUA->Top() >= 2 ){
		navArea->UnblockArea( LUA->GetNumber( 2 ) );
	} else {
		navArea->UnblockArea();
	}

	LUA->PushBool( true );
	return 1;
}