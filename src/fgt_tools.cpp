#ifdef WIN32
	#include <windows.h>
#else
	#include <dlfcn.h>
	#include <sys/types.h>
	#include <sys/stat.h> 
#endif

#include "fgt_tools.h"
#include "eiface.h"
#include "engine/IEngineTrace.h"
extern IEngineTrace *enginetrace;

void FGT_TraceLine( const Vector& vecAbsStart, const Vector& vecAbsEnd, unsigned int mask, const IHandleEntity *ignore, int collisionGroup, trace_t *ptr ){
	if( !enginetrace ){
		Warning( "FgtNavMesh::FGT_TraceLine called while enginetrace == NULL!\n" );
		return;
	}
	Ray_t ray;
	ray.Init( vecAbsStart, vecAbsEnd );
	//CTraceFilterSimple traceFilter( ignore, collisionGroup );
	CTraceFilterWorldOnly traceFilter;

	enginetrace->TraceRay( ray, mask, &traceFilter, ptr );
}
void FGT_TraceLine( const Vector& vecAbsStart, const Vector& vecAbsEnd, unsigned int mask, ITraceFilter *pFilter, trace_t *ptr ){
	if( !enginetrace ){
		Warning( "FgtNavMesh::FGT_TraceLine called while enginetrace == NULL!\n" );
		return;
	}
	Ray_t ray;
	ray.Init( vecAbsStart, vecAbsEnd );

	enginetrace->TraceRay( ray, mask, pFilter, ptr );
}

// http://www.sourcemod.net/devlog/?p=57
unsigned int* FgtFindSignature( unsigned char *pBaseAddress, unsigned int baseLength, const char *pSignature, unsigned int sigLength ) {
	unsigned char *pBasePtr = pBaseAddress;
	unsigned char *pEndPtr = pBaseAddress + baseLength;
	unsigned int i;
	//Msg( "Scanning from 0x%x to 0x%x\n", pBasePtr, pEndPtr );
	while( pBasePtr < pEndPtr ){
		for( i = 0; i < sigLength; i++ ){
			if( pSignature[i] != '*' && (pSignature[i] & 0xFF) != pBasePtr[i] ){ // * (\x2A) is our wildcard to match anything
				break;
			}
		}
		if( i == sigLength - 1 ){ // Don't forget the null terminator
			//Msg( "Full Match pBasePtr: 0x%x\n", pBasePtr );
			return (unsigned int*)pBasePtr;
		}
		pBasePtr += sizeof(unsigned char);
	}
	return NULL;
}

// http://www.sourcemod.net/devlog/?p=56
bool FgtGetDllInfo( void *searchAddr, unsigned char **pBaseAddr, unsigned int *memSize ){
#ifdef WIN32
	MEMORY_BASIC_INFORMATION mem;
	if( !VirtualQuery( searchAddr, &mem, sizeof( mem ) ) ){
		return false;
	}
	if( *pBaseAddr ){
		*pBaseAddr = (unsigned char*)mem.AllocationBase;
	}

	IMAGE_DOS_HEADER *dos = (IMAGE_DOS_HEADER*)mem.AllocationBase;
	IMAGE_NT_HEADERS *pe = (IMAGE_NT_HEADERS*)((unsigned long)dos + (unsigned long)dos->e_lfanew); // e_lfanew = File address of new exe header
	if( pe->Signature != IMAGE_NT_SIGNATURE ){
		return false;
	}
	if( memSize ){
		*memSize = (unsigned long)( pe->OptionalHeader.SizeOfImage ); // The size of the image, in bytes, including all headers. Must be a multiple of SectionAlignment.
	}
	return true;
#else
	Dl_info info;
	struct stat buf;

	if( !dladdr( searchAddr, &info) )
		return false;

	if( !info.dli_fbase || !info.dli_fname )
		return false;

	if( stat( info.dli_fname, &buf ) != 0 )
		return false;

	*pBaseAddr = (unsigned char*)info.dli_fbase;
	*memSize = buf.st_size;
#endif
}
