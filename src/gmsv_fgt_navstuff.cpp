#if defined( WIN32 )
	#undef _UNICODE
	#define WIN32_LEAN_AND_MEAN
	#include <windows.h>
#elif defined( LINUX )
#endif

#define _ALLOW_KEYWORD_MACROS
#define private public // Call the police, I don't give a fuck.

#include "GarrysMod/Lua/Interface.h"

#include <stdio.h>
#include <cstdlib>
#include <string>
#include <vector>

#include <cbase.h>
#include <interface.h>
#include "eiface.h"
#include <convar.h>
#include <nav_area.h>
#include <nav_mesh.h>

#include "signatures.h"
#include "allied_sigscan/sigscan.h"
#include "fgt_classes/fgtnavmesh.h"
#include "fgt_classes/fgtnavarea.h"
#include "cnavarea_functions.h"
#include "cnavmesh_functions.h"
#include "fgt_tools.h"

using namespace GarrysMod::Lua;
bool g_FgtNavstuffLoaded = false;
int CNavAreaM_RefID = NULL;
int PlayerMeta_RefID = NULL;
int EntityMeta_RefID = NULL;
int VectorMeta_RefID = NULL;
lua_State *g_LuaState = NULL;
CNavMesh *g_CNavMesh = NULL;
FgtNavMesh *g_NavMesh = NULL;
extern ICvar *g_ICvar;
extern IVEngineServer *engine;

// Signatures
CSigScan CNavMesh_Sig;

// ConCommands
static ConCommand *fgt_navstuff_unittest = NULL;

int testLuaFunc( lua_State *state ){
	UserData *ud = (UserData*)LUA->GetUserdata( 1 );
	Msg( "ud: type %i : data 0x%x\n", ud->type, ud->data );

	CBaseHandle hent = *(CBaseHandle*)ud->data;

	Msg( "hent: 0x%x\n", &hent );
	if( !hent.IsValid() ){
		Warning( "Failed to find ent\n" );
		return 0;
	}
	edict_t *ent = engine->PEntityOfEntIndex( hent.GetEntryIndex() );
	Msg( "%s\n", ent->GetUnknown()->GetBaseEntity()->GetModelName().ToCStr() );

	return 0;
}

void fgtNavUnitTest( const CCommand &command ){
	if( !g_NavMesh || !g_CNavMesh ){
		Warning( "g_NavMesh IS NULL! [0x%x]\n", g_NavMesh );
		return;
	}

	// GetAllNavAreas
	Msg( "Calling g_NavMesh->GetAllNavAreas()...\n" );
	std::vector< CNavArea* > areas = g_NavMesh->GetAllNavAreas();
	Msg( "\t Returned vector has a size of %i\n", areas.size() );

	// GetNearestNavArea
	Msg( "Calling g_NavMesh->GetNearestNavArea at worldspawn with no LOS checking...\n" );
	CNavArea *nearestArea = g_NavMesh->GetNearestNavArea( Vector(0,0,0), true, 10000.0f, false, true, TEAM_ANY );
	if( nearestArea ){
		Msg( "\t Closest navarea found with an ID of %i\n", nearestArea->GetID() );
	} else {
		Msg( "\t returned NULL (No navarea withing 10000 units of 0, 0, 0 ?)\n" );
	}

	// Get area to use in cnavarea unit tests from getallnavareas
	FgtNavArea *area = (FgtNavArea*)areas[0];
	if( !area ){
		Msg( "areas[0] returned nothing. No navmesh on this map?\n Skipping NavArea methods test.\n" );
		return;
	}
	Msg( "\nUsing navarea %i as base for next tests\n", area->GetID() );

	// IsValid
	Msg( "Calling IsValid (A.K.A GetID) ... " );
	Msg( "%s\n", area->GetID() ? "TRUE" : "FALSE" );

	// GetAttributes
	Msg( "Calling GetAttributes... " );
	Msg( "%i\n", area->GetAttributes() );

	// GetAdjacentCount
	Msg( "Calling GetAdjacentCount... " );
	Msg( "North: %i\n", area->GetAdjacentCount( NORTH ) );

	// GetAdjacentArea
	Msg( "Calling GetAdjacentArea... " );
	Msg( "ID: %i\n", area->GetAdjacentArea( NORTH, 0 ) ? area->GetAdjacentArea( NORTH, 0 )->GetID() : -1 );

	// GetRandomAdjacentArea
	Msg( "Calling GetRandomAdjacentArea... " );
	CNavArea *tArea = area->GetRandomAdjacentArea( NORTH );
	Msg( "ID: %i\n", tArea ? tArea->GetID() : -1 );

	// CollectAdjacentAreas
	Msg( "Calling CollectAdjacentAreas... " );
	std::vector< CNavArea* > adjAreas;
	area->CollectAdjacentAreas( &adjAreas );
	Msg( "Size: %i\n", adjAreas.size() );
	adjAreas.clear(); // I feel like I need to call the deconstructor here for some reason...

	// IsConnected
	Msg( "Calling IsConnected... " );
	Msg( "%s\n", area->IsConnected( area, NUM_DIRECTIONS ) ? "TRUE" : "FALSE" );

	// ComputeGroundHeightChange
	Msg( "Calling ComputeGroundHeightChange... " );
	Msg( "%i\n", area->ComputeGroundHeightChange( area ) );

	// ComputeDirection
	Msg( "Calling ComputeDirection... " );
	Vector vec = Vector( 0, 0, 0 );
	Msg( "%i\n", area->ComputeDirection( &vec ) );

	// GetPlayerCount
	Msg( "Calling GetPlayerCount... " );
	Msg( "%i\n", area->GetPlayerCount( TEAM_ANY ) );

	// IsOverlappingArea
	Msg( "Calling IsOverlappingArea... " );
	Msg( "%s\n", area->IsOverlappingArea( area ) ? "TRUE" : "FALSE" );

	Msg( "Calling IsBlocked... " );
	Msg( "%s\n", area->IsBlocked( TEAM_ANY ) ? "TRUE" : "FALSE" );

	Msg( "Calling MarkAsBlocked... " );
	edict_t* worldEdict = engine->PEntityOfEntIndex( 0 );
	CBaseEntity* worldEnt = worldEdict->GetUnknown()->GetBaseEntity();
	area->MarkAsBlocked( TEAM_ANY, worldEnt );
	Msg( "%s\n", area->IsBlocked( TEAM_ANY ) ? "TRUE" : "FALSE" );

	Msg( "Calling UnblockArea... " );
	area->UnblockArea();
	Msg( "%s\n", area->IsBlocked( TEAM_ANY ) ? "TRUE" : "FALSE" );

	Msg( "Unit test done.\n" );
}

GMOD_MODULE_OPEN() {
	//Msg( "fgt_navstuff gmod_module_open\n" );
	g_LuaState = state;

	unsigned int *navPtr = NULL;
	if( !g_ICvar || !enginetrace ){
		Warning( "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n" );
		Warning( "        gmsv_fgt_navstuff isn't being loaded as a source plugin!\n" );
		//Warning( "            Some of the bindings will not work properly!\n" ); // If I decide to allow CNavArea metamethods when not loaded as plugin.
		Warning( "           Please check your vdf file in the addons folder\n" );
		Warning( "            For more information visit http://goo.gl/dgcCfZ \n" );
		Warning( "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n" );
		return 0;
	} else {
		// TODO: Remove this test concommand.
		fgt_navstuff_unittest = new ConCommand( "fgt_navstuff_unittest", fgtNavUnitTest, "", FCVAR_UNREGISTERED );
		g_ICvar->RegisterConCommand( fgt_navstuff_unittest );
		
		// Find the address of the nav_use_place callback function.
		// Inside it is our pointer to the CNavMesh we want.
		ConCommand *nav_use_place = g_ICvar->FindCommand( "nav_use_place" );
		if( !nav_use_place ){ // I have no idea how this is happening...
			Warning( "Failed to get nav_use_place!\n" );
			return 0;
		}
		void *func_nav_use_place = (void*)nav_use_place->m_fnCommandCallbackV1;
		// We use FgtFindSignature becuase we only want to scan inside the nav_use_place concommand and not the whole server.dll
		navPtr = FgtFindSignature( (unsigned char*)func_nav_use_place, 0x94, CNAVMESH_SIG, sizeof( CNAVMESH_SIG ) );
	}

	if( !navPtr ){
		Warning( "\n ~~~ FAILED TO FIND CNAVMESH POINTER IN NAV_USE_PLACE! ~~~ \n" );
		Warning( "          gmsv_fgt_navstuff failed to load!\n\n" );

	} else {
		// We have a pointer to TheNavMesh we can setup the lua functions now.
#ifdef WIN32
		g_CNavMesh = (CNavMesh*) ( **(unsigned int **)( (unsigned int)navPtr + 0x2 ) );
#elif defined( LINUX )
		g_CNavMesh = (CNavMesh*) ( **(unsigned int **)( (unsigned int)navPtr + 0x1 ) );
#endif
		g_NavMesh = (FgtNavMesh*) g_CNavMesh;

		LUA->PushSpecial( SPECIAL_GLOB ); // _G
			LUA->PushCFunction( testLuaFunc );
			LUA->SetField( -2, "FGTTEST" );

			LUA->PushNumber( 0 );
			LUA->SetField( -2, "NAVATTR_INVALID" ); // TODO: Add all the NavAttributeTypes in nav.h
			// Anytime I use CNavArea::GetAttributes it returns 0 though. (Haven't tested with a no-qucksave navmesh).

			LUA->PushNumber( 0 ); // NavDirTypes
			LUA->SetField( -2, "NAVDIR_NORTH" );
			LUA->PushNumber( 1 );
			LUA->SetField( -2, "NAVDIR_EAST" );
			LUA->PushNumber( 2 );
			LUA->SetField( -2, "NAVDIR_SOUTH" );
			LUA->PushNumber( 3 );
			LUA->SetField( -2, "NAVDIR_WEST" );
			// TODO: Have a NAVDIR_ALL = { 0, 1, 2, 3 } ?

			LUA->PushNumber( 0 ); // NavCornerTypes
			LUA->SetField( -2, "NAVCORNER_NW" );
			LUA->PushNumber( 1 );
			LUA->SetField( -2, "NAVCORNER_NE" );
			LUA->PushNumber( 2 );
			LUA->SetField( -2, "NAVCORNER_SE" );
			LUA->PushNumber( 3 );
			LUA->SetField( -2, "NAVCORNER_SW" );

			LUA->GetField( -1, "navmesh" ); // _G.navmesh
				LUA->PushCFunction( NavMesh_GetAllNavAreas );
				LUA->SetField( -2, "GetAllNavAreas" );

				LUA->PushCFunction( NavMesh_GetNearestNavArea );
				LUA->SetField( -2, "GetNearestNavArea" );
			LUA->Pop(); // navmesh
		LUA->Pop(); // _G

		LUA->PushSpecial( SPECIAL_REG ); // _R
		LUA->GetField( -1, "Player" );
			PlayerMeta_RefID = LUA->ReferenceCreate(); // Create a reference to the Player metatable.

		LUA->GetField( -1, "Entity" );
			EntityMeta_RefID = LUA->ReferenceCreate(); // Create a reference to the Entity metatable.

		LUA->GetField( -1, "Vector" );
			VectorMeta_RefID = LUA->ReferenceCreate(); // Create a reference to the Vector metatable.

		LUA->GetField( -1, "CNavArea" ); // _R.CNavArea
			CNavAreaM_RefID = LUA->ReferenceCreate(); // Create a reference to the _R.CNavArea metatable. (This pops the stack)
			LUA->ReferencePush( CNavAreaM_RefID ); // Push the metatable back onto the stack with the reference we just created.

			LUA->PushCFunction( CNavArea_IsValid ); // CNavArea:IsValid()
			LUA->SetField( -2, "IsValid" );

			LUA->PushCFunction( CNavArea_GetAttributes ); // CNavArea:GetAttributes()
			LUA->SetField( -2, "GetAttributes" );

			LUA->PushCFunction( CNavArea_GetAdjacentCount );
			LUA->SetField( -2, "GetAdjacentCount" ); // CNavArea:GetAdjacentCount( direction )

			LUA->PushCFunction( CNavArea_GetAdjacentArea );
			LUA->SetField( -2, "GetAdjacentArea" ); // CNavArea:GetAdjacentArea( Direction, Number )

			LUA->PushCFunction( CNavArea_GetRandomAdjacentArea ); // CNavArea:GetRandomAdjacentArea( Direction )
			LUA->SetField( -2, "GetRandomAdjacentArea" );

			LUA->PushCFunction( CNavArea_CollectAdjacentAreas ); // CNavArea:CollectAdjacentAreas()
			LUA->SetField( -2, "CollectAdjacentAreas" );

			LUA->PushCFunction( CNavArea_IsConnected ); // CNavArea:IsConnected( CNavArea, Direction )
			LUA->SetField( -2, "IsConnected" );

			LUA->PushCFunction( CNavArea_ComputeGroundHeightChange );
			LUA->SetField( -2, "ComputeGroundHeightChange" );

			// TODO: GetIncomingConnections
			// TODO: GetDanger

			LUA->PushCFunction( CNavArea_IsBlocked );
			LUA->SetField( -2, "IsBlocked" );

			LUA->PushCFunction( CNavArea_MarkAsBlocked );
			LUA->SetField( -2, "MarkAsBlocked" );

			LUA->PushCFunction( CNavArea_UnblockArea );
			LUA->SetField( -2, "UnblockArea" );

#ifdef LINUX // It crashes in windows on GC or something. I don't give a fuck about windows anymore.
			LUA->PushCFunction( CNavArea_GetCenter );
			LUA->SetField( -2, "GetCenter" );
#endif

			// TODO: ComputeNormal
			LUA->PushCFunction( CNavArea_ComputeDirection );
			LUA->SetField( -2, "ComputeDirection" );
		
			LUA->PushCFunction( CNavArea_GetPlayerCount ); // CNavArea:GetPlayerCount( TeamID )
			LUA->SetField( -2, "GetPlayerCount" );

			// TODO: GetLightIntensity
			// TODO: Is*Visible()

			LUA->PushCFunction( CNavArea_IsOverlappingArea );
			LUA->SetField( -2, "IsOverlappingArea" );

		LUA->Pop( 2 ); // Pop CNavArea and _R
		g_FgtNavstuffLoaded = true;
		Msg( "fgt_navstuff loaded successfully!\n" );
	}
	return 0;
}


GMOD_MODULE_CLOSE() {
	//Msg( "fgt_navstuff gmod_module_close\n" );
	if( CNavAreaM_RefID ){
		LUA->ReferenceFree( CNavAreaM_RefID ); // Free our reference to the CNavArea metatable.
		CNavAreaM_RefID = NULL;
	}
	if( PlayerMeta_RefID ){
		LUA->ReferenceFree( PlayerMeta_RefID );
		PlayerMeta_RefID = NULL;
	}
	if( EntityMeta_RefID ){
		LUA->ReferenceFree( EntityMeta_RefID );
		EntityMeta_RefID = NULL;
	}
	if( VectorMeta_RefID ){
		LUA->ReferenceFree( VectorMeta_RefID );
		VectorMeta_RefID = NULL;
	}

	if( fgt_navstuff_unittest ){
		g_ICvar->UnregisterConCommand( fgt_navstuff_unittest );
		delete fgt_navstuff_unittest;
	}
	g_FgtNavstuffLoaded = false;
	return 0;
}
