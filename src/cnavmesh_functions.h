#include "GarrysMod/Lua/Interface.h"

using namespace GarrysMod::Lua;
extern int CNavAreaM_RefID;
extern FgtNavMesh *g_NavMesh;

int NavMesh_GetAllNavAreas( lua_State *state ){
	std::vector< CNavArea* > areas;
	areas = g_NavMesh->GetAllNavAreas();

	LUA->CreateTable();

	for( unsigned int i = 1; i <= areas.size(); i++ ){
		LUA->PushNumber( i ); // K
			
		CNavArea *area = areas[ i - 1 ]; // V
		UserData *retUd = (UserData*)LUA->NewUserdata( sizeof( UserData ) );
		retUd->data = area;
		retUd->type = 37;
		LUA->ReferencePush( CNavAreaM_RefID );
		LUA->SetMetaTable( -2 );

		LUA->SetTable( -3 ); // table[ i ] = nav
	}

	return 1;
}

// This function doesn't require all paramters to be set.
int NavMesh_GetNearestNavArea( lua_State *state ){
	LUA->CheckType( 1, Type::VECTOR ); // First argument is required and will always be a vector
	UserData *ud = (UserData*)LUA->GetUserdata( 1 );
	Vector *vec = (Vector*)ud->data;

	int numArgs = LUA->Top(); // We need to keep track of how many arguments are being passed.
	// Arg1 vector is defined above
	bool anyZ = false; // Arg2 (This has no effect but valve still has it defined.)
	float maxDist = 10000.0f; // Arg3
	bool checkLOS = false; // Arg4
	bool checkGround = true; // Arg5
	int team = TEAM_ANY; // Arg6

	if( numArgs >= 2 ){
		LUA->CheckType( 2, Type::BOOL );
		anyZ = LUA->GetBool( 2 );
		// I thought about putting a if( numArgs == 2 ) goto postcheck to avoid checking the other statements
		// But I figured the lynching for using goto wouldn't be worth it. (And the compiler probably optimizes it anyway).
	}
	if( numArgs >= 3 ){
		LUA->CheckType( 3, Type::NUMBER );
		maxDist = LUA->GetNumber( 3 );
	}
	if( numArgs >= 4 ){
		LUA->CheckType( 4, Type::BOOL );
		checkLOS = LUA->GetBool( 4 );
	}
	if( numArgs >= 5 ){
		LUA->CheckType( 5, Type::BOOL );
		checkGround = LUA->GetBool( 5 );
	}
	if( numArgs == 6 ){
		LUA->CheckType( 6, Type::NUMBER );
		team = LUA->GetNumber( 6 );
	} else if( numArgs > 6 ) {
		LUA->ArgError( 7, "Too many arguments passed to function" );
	}

	// postcheck: would be here if everyone didn't hate goto statements
	CNavArea *area = g_NavMesh->GetNearestNavArea( *vec, anyZ, maxDist, checkLOS, checkGround, team );

	if( area ){
		UserData *retUd = (UserData*)LUA->NewUserdata( sizeof( UserData ) );
		retUd->data = area;
		retUd->type = 37;
		LUA->ReferencePush( CNavAreaM_RefID );
		LUA->SetMetaTable( -2 );
	} else {
		LUA->PushNil();
	}
	return 1;
}