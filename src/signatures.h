#if defined( WIN32 )
	// CNAVMESH signature can be found by getting the TheNavMesh external from CommandNavUsePlace/nav_use_place concommand. See nav_mesh.cpp:2339
	// Look for `mov eax, TheNavMesh`
	#define CNAVMESH_SIG "\x8B\x0D\x2A\x2A\x2A\x2A\x5D\xE9"
	#define CNAVMESH_SIGMASK "xx????xx"

	// GETNEARESTNAV signature can be found by putting a breakpoint on RandomInt with a break condition. [ESP+8] = 1024*1024. See nav_mesh.cpp:865
	// This signature is no longer needed. Just keeping it here in case its useful later on.
	#define GETNEARESTNAV_SIG "\x55\x8B\x2A\x83\xEC\x18\x56\x8B\x2A\x83\x2A\x2A\x2A\x89"
	#define GETNEARESTNAV_SIGMASK "xx?xxxxx?x???x"

#elif defined( LINUX )
	#define CNAVMESH_SIG "\xA1\x2A\x2A\x2A\x2A\x89\x2A\x2A\xE8"
	#define CNAVMESH_SIGMASK "x????x??x"

#endif
