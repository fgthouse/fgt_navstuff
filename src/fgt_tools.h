#ifndef FgtTools_H
#define FgtTools_H
#include "eiface.h"

void FGT_TraceLine( const Vector& vecAbsStart, const Vector& vecAbsEnd, unsigned int mask, const IHandleEntity *ignore, int collisionGroup, trace_t *ptr );
void FGT_TraceLine( const Vector& vecAbsStart, const Vector& vecAbsEnd, unsigned int mask, ITraceFilter *pFilter, trace_t *ptr );

unsigned int* FgtFindSignature( unsigned char *pBaseAddress, unsigned int baseLength, const char *pSignature, unsigned int sigLength );
bool FgtGetDllInfo( void *searchAddr, unsigned char **pBaseAddr, unsigned int *memSize );

#endif
