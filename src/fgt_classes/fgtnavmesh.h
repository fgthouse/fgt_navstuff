#ifndef FgtNavMesh_H
#define FgtNavMesh_H

#define _ALLOW_KEYWORD_MACROS
#define private public // Call the police, I don't give a fuck.

#include <cbase.h>
#include <nav_mesh.h>
#include "fgtnavarea.h"
#ifdef LINUX
	#undef min
	#undef max
#endif
#include <vector>

class FgtNavMesh : public CNavMesh {
	public:
		CNavArea* GetNearestNavArea( const Vector &pos, bool anyZ = false, float maxDist = 10000.0f, bool checkLOS = false, bool checkGround = true, int team = TEAM_ANY );
		CNavArea* GetNavArea( const Vector &pos, float beneathLimit = 120.0f );
		CNavArea* GetNavAreaByID( unsigned int id );
		bool GetGroundHeight( const Vector &pos, float *height, Vector *normal = NULL );
		const char* GetPlayerSpawnName( void );
		float GetCellSize( void );
		std::vector< CNavArea* > GetAllNavAreas( void );
		bool GetSimpleGroundHeight( const Vector &pos, float *height, Vector *normal = NULL );
};

#endif
