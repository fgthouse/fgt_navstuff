#include "fgtnavmesh.h"
#include "../fgt_tools.h"
#include "../signatures.h"
#include "engine/IEngineTrace.h"

CNavArea* FgtNavMesh::GetNavArea( const Vector &pos, float beneathLimit ){
	if ( !m_grid.Count() )
		return NULL;

	// get list in cell that contains position
	int x = WorldToGridX( pos.x );
	int y = WorldToGridY( pos.y );
	NavAreaVector *areaVector = &m_grid[ x + y*m_gridSizeX ];

	// search cell list to find correct area
	CNavArea *use = NULL;
	float useZ = -99999999.9f;
	Vector testPos = pos + Vector( 0, 0, 5 );

	FOR_EACH_VEC( (*areaVector), it ) {
		//CNavArea *area = (*areaVector)[ it ];
		FgtNavArea *area = (FgtNavArea*)(*areaVector)[ it ];

		// check if position is within 2D boundaries of this area
		if (area->IsOverlapping( testPos )) {
			// project position onto area to get Z
			float z = area->GetZ( testPos );

			// if area is above us, skip it
			if (z > testPos.z){
				continue;
			}

			// if area is too far below us, skip it
			if (z < pos.z - beneathLimit){
				continue;
			}

			// if area is higher than the one we have, use this instead
			if (z > useZ) {
				use = area;
				useZ = z;
			}
		}
	}
	return use;
}


CNavArea* FgtNavMesh::GetNavAreaByID( unsigned int id ){
	// TODO: m_hashTable is being weird. Try and understand it.
	if (id == 0){
		return NULL;
	}
	std::vector< CNavArea* > allAreas = GetAllNavAreas();
	for( unsigned int i = 0; i < allAreas.size(); i++ ){
		if( allAreas[ i ]->GetID() == id ){
			return allAreas[ i ];
		}
	}
	return NULL;
}

// I mixed GetGroundHeightSimple in here.
bool FgtNavMesh::GetGroundHeight( const Vector &pos, float *height, Vector *normal ){
	const float flMaxOffset = 100.0f;

	//CTraceFilterGroundEntities filter( NULL, COLLISION_GROUP_NONE, WALK_THRU_EVERYTHING );

	trace_t result;
	Vector to( pos.x, pos.y, pos.z - 10000.0f );
	Vector from( pos.x, pos.y, pos.z + HalfHumanHeight + 1e-3 );
	while( to.z - pos.z < flMaxOffset ) 
	{
		//UTIL_TraceLine( from, to, MASK_NPCSOLID_BRUSHONLY, &filter, &result );
		FGT_TraceLine( from, to, MASK_NPCSOLID_BRUSHONLY, NULL, COLLISION_GROUP_NONE, &result );
		if ( !result.startsolid && (( result.fraction == 1.0f ) || ( ( from.z - result.endpos.z ) >= HalfHumanHeight ) ) ){
			*height = result.endpos.z;
			if ( normal )
			{
				*normal = !result.plane.normal.IsZero() ? result.plane.normal : Vector( 0, 0, 1 );
			}
			return true;
		}	  
		to.z = ( result.startsolid ) ? from.z : result.endpos.z;
		from.z = to.z + HalfHumanHeight + 1e-3;
	}

	*height = 0.0f;
	if ( normal ){
		normal->Init( 0.0f, 0.0f, 1.0f );
	}
	return false;
}

// see nav_mesh.cpp:824
CNavArea* FgtNavMesh::GetNearestNavArea( const Vector &pos, bool anyZ, float maxDist, bool checkLOS, bool checkGround, int team ) {
	if ( !m_grid.Count() )
		return NULL;	

	CNavArea *close = NULL;
	float closeDistSq = maxDist * maxDist;

	// quick check
	if ( !checkLOS && !checkGround ){
		close = GetNavArea( pos );
		if ( close )
		{
			return close;
		}
	}

	// ensure source position is well behaved
	Vector source;
	source.x = pos.x;
	source.y = pos.y;
	if ( GetGroundHeight( pos, &source.z ) == false ){
		if ( !checkGround ){
			source.z = pos.z;
		} else {
			return NULL;
		}
	}

	source.z += HalfHumanHeight;

	// find closest nav area

	// use a unique marker for this method, so it can be used within a SearchSurroundingArea() call
	static unsigned int searchMarker = RandomInt( 0, 1024*1024 );

	++searchMarker;

	if ( searchMarker == 0 ){
		++searchMarker;
	}


	// get list in cell that contains position
	int originX = WorldToGridX( pos.x );
	int originY = WorldToGridY( pos.y );

	int shiftLimit = ceil(maxDist / m_gridCellSize);

	//
	// Search in increasing rings out from origin, starting with cell
	// that contains the given position.
	// Once we find a close area, we must check one more step out in
	// case our position is just against the edge of the cell boundary
	// and an area in an adjacent cell is actually closer.
	// 
	for( int shift=0; shift <= shiftLimit; ++shift ){
		for( int x = originX - shift; x <= originX + shift; ++x ){
			if ( x < 0 || x >= m_gridSizeX )
				continue;

			for( int y = originY - shift; y <= originY + shift; ++y ){
				if ( y < 0 || y >= m_gridSizeY )
					continue;

				// only check these areas if we're on the outer edge of our spiral
				if ( x > originX - shift &&
					 x < originX + shift &&
					 y > originY - shift &&
					 y < originY + shift )
					continue;

				NavAreaVector *areaVector = &m_grid[ x + y*m_gridSizeX ];

				// find closest area in this cell
				FOR_EACH_VEC( (*areaVector), it ){
					FgtNavArea *area = (FgtNavArea*)(*areaVector)[ it ];

					// skip if we've already visited this area
					if( area->GetNearNavSearchMarker() == searchMarker ){
						continue;
					}

					// don't consider blocked areas
					if ( area->IsBlocked( team ) )
						continue;

					// mark as visited
					area->GetNearNavSearchMarker() = searchMarker;

					Vector areaPos;
					area->GetClosestPointOnArea( source, &areaPos );

					// TERROR: Using the original pos for distance calculations.  Since it's a pure 3D distance,
					// with no Z restrictions or LOS checks, this should work for passing in bot foot positions.
					// This needs to be ported back to CS:S.
					float distSq = ( areaPos - pos ).LengthSqr();

					// keep the closest area
					if ( distSq >= closeDistSq )
						continue;

					// check LOS to area
					// REMOVED: If we do this for !anyZ, it's likely we wont have LOS and will enumerate every area in the mesh
					// It is still good to do this in some isolated cases, however
					if ( checkLOS ){
						trace_t result;

						// make sure 'pos' is not embedded in the world
						Vector safePos;

						FGT_TraceLine( pos, pos + Vector( 0, 0, StepHeight ), MASK_NPCSOLID_BRUSHONLY, NULL, COLLISION_GROUP_NONE, &result );
						if ( result.startsolid ){
							// it was embedded - move it out
							safePos = result.endpos + Vector( 0, 0, 1.0f );
						} else {
							safePos = pos;
						}

						// Don't bother tracing from the nav area up to safePos.z if it's within StepHeight of the area, since areas can be embedded in the ground a bit
						float heightDelta = fabs(areaPos.z - safePos.z);
						if ( heightDelta > StepHeight ){
							// trace to the height of the original point
							FGT_TraceLine( areaPos + Vector( 0, 0, StepHeight ), Vector( areaPos.x, areaPos.y, safePos.z ), MASK_NPCSOLID_BRUSHONLY, NULL, COLLISION_GROUP_NONE, &result );
							
							if ( result.fraction != 1.0f ){
								continue;
							}
						}

						// trace to the original point's height above the area
						FGT_TraceLine( safePos, Vector( areaPos.x, areaPos.y, safePos.z + StepHeight ), MASK_NPCSOLID_BRUSHONLY, NULL, COLLISION_GROUP_NONE, &result );

						if ( result.fraction != 1.0f ){
							continue;
						}
					}

					closeDistSq = distSq;
					close = area;

					// look one more step outwards
					shiftLimit = shift+1;
				}
			}
		}
	}

	return close;
}

const char* FgtNavMesh::GetPlayerSpawnName( void ){
	if( m_spawnName ){
		return m_spawnName;
	}
	return "info_player_start";
}

float FgtNavMesh::GetCellSize( void ){
	return m_gridCellSize;
}

std::vector< CNavArea* > FgtNavMesh::GetAllNavAreas( void ){
	std::vector< CNavArea* > areas;
	FOR_EACH_VEC( m_grid, it ){
		FOR_EACH_VEC( m_grid[ it ], i ){
			areas.push_back( m_grid[ it ][ i ] );
		}
	}
	return areas;
}

bool FgtNavMesh::GetSimpleGroundHeight( const Vector &pos, float *height, Vector *normal ){
	Vector to;
	to.x = pos.x;
	to.y = pos.y;
	to.z = pos.z - 9999.9f;

	trace_t result;

	FGT_TraceLine( pos, to, MASK_NPCSOLID_BRUSHONLY, NULL, COLLISION_GROUP_NONE, &result );

	if (result.startsolid)
		return false;

	*height = result.endpos.z;

	if (normal)
		*normal = result.plane.normal;

	return true;
}
