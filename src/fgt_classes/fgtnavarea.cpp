#include <cbase.h>
#include "fgtnavarea.h"
#include "fgtnavmesh.h"
extern FgtNavMesh *g_NavMesh;

// This is for use in fgtnavmesh.h:GetNearestNavArea. It passes m_nearNavSearchMarker by reference.
unsigned int& FgtNavArea::GetNearNavSearchMarker(){
	return m_nearNavSearchMarker;
}

float FgtNavArea::GetZ( float x, float y ){
	if (m_invDxCorners == 0.0f || m_invDyCorners == 0.0f)
		return m_neZ;

	float u = (x - m_nwCorner.x) * m_invDxCorners;
	float v = (y - m_nwCorner.y) * m_invDyCorners;

	// clamp Z values to (x,y) volume
	
	u = fsel( u, u, 0 );			// u >= 0 ? u : 0
	u = fsel( u - 1.0f, 1.0f, u );	// u >= 1 ? 1 : u

	v = fsel( v, v, 0 );			// v >= 0 ? v : 0
	v = fsel( v - 1.0f, 1.0f, v );	// v >= 1 ? 1 : v

	float northZ = m_nwCorner.z + u * (m_neZ - m_nwCorner.z);
	float southZ = m_swZ + u * (m_seCorner.z - m_swZ);

	return northZ + v * (southZ - northZ);
}
float FgtNavArea::GetZ( Vector vec ){
	return GetZ( vec.x, vec.y );
}

void FgtNavArea::GetClosestPointOnArea( const Vector * RESTRICT pPos, Vector *close ){
        float x, y, z;

        // Using fsel rather than compares, as much faster on 360 [7/28/2008 tom]
        x = fsel( pPos->x - m_nwCorner.x, pPos->x, m_nwCorner.x );
        x = fsel( x - m_seCorner.x, m_seCorner.x, x );

        y = fsel( pPos->y - m_nwCorner.y, pPos->y, m_nwCorner.y );
        y = fsel( y - m_seCorner.y, m_seCorner.y, y );

        z = GetZ( x, y );

        close->Init( x, y, z );
}

bool FgtNavArea::IsOverlapping( const Vector &pos, float tolerance ){
	if (pos.x + tolerance >= m_nwCorner.x && pos.x - tolerance <= m_seCorner.x &&
		pos.y + tolerance >= m_nwCorner.y && pos.y - tolerance <= m_seCorner.y)
		return true;

	return false;
}

CNavArea* FgtNavArea::GetRandomAdjacentArea( NavDirType dir ){
	int count = m_connect[ dir ].Count();
	int which = RandomInt( 0, count-1 );

	int i = 0;
	FOR_EACH_VEC( m_connect[ dir ], it ){
		if( i == which ){
			return m_connect[ dir ][ it ].area;
		}
		++i;
	}
	return NULL;
}

void FgtNavArea::CollectAdjacentAreas( std::vector< CNavArea* > *adjVector ){
	for( int d = 0; d < NUM_DIRECTIONS; ++d ){
		FOR_EACH_VEC( m_connect[ d ], it ){
			adjVector->push_back( m_connect[ d ].Element( it ).area );
		}

		const NavConnectVector *incoming = this->GetIncomingConnections( (NavDirType)d ); // Include one way connections
		FOR_EACH_VEC( *incoming, it ){
			NavConnect connection = (*incoming)[ it ];
			//Msg( "incoming %i\n", connection.area->GetID() );
			adjVector->push_back( connection.area );
		}
	}
}

bool FgtNavArea::IsConnected( const CNavArea *area, NavDirType dir ){
	if( area == this ){ // Connected to ourself
		return true;
	}

	if( dir == NUM_DIRECTIONS ){ // Search all directions
		for( int d = 0; d < NUM_DIRECTIONS; ++d ){
			FOR_EACH_VEC( m_connect[ d ], it ){
				if( area == m_connect[ d ][ it ].area ){
					return true;
				}
			}
		}
		// Check ladder connections needed here?
	} else {
		FOR_EACH_VEC( m_connect[ dir ], it ){
			if( area == m_connect[ dir ][ it ].area ){
				return true;
			}
		}
	}
	return false;
}

float FgtNavArea::ComputeGroundHeightChange( FgtNavArea *area ){
	Vector closeFrom, closeTo;
	area->GetClosestPointOnArea( GetCenter(), &closeTo );
	GetClosestPointOnArea( area->GetCenter(), &closeFrom );

	float toZ, fromZ;
	if ( g_NavMesh->GetSimpleGroundHeight( closeTo + Vector( 0, 0, StepHeight ), &toZ ) == false ){
		return 0.0f;
	}
	if ( g_NavMesh->GetSimpleGroundHeight( closeFrom + Vector( 0, 0, StepHeight ), &fromZ ) == false ){
		return 0.0f;
	}

	return toZ - fromZ;
}

NavDirType FgtNavArea::ComputeDirection( Vector *point ){
	if (point->x >= m_nwCorner.x && point->x <= m_seCorner.x){
		if (point->y < m_nwCorner.y){
			return NORTH;
		} else if (point->y > m_seCorner.y) {
			return SOUTH;
		}

	} else if (point->y >= m_nwCorner.y && point->y <= m_seCorner.y) {
		if (point->x < m_nwCorner.x){
			return WEST;
		} else if (point->x > m_seCorner.x) {
			return EAST;
		}
	}

	// find closest direction
	Vector to = *point - m_center;

	if (fabs(to.x) > fabs(to.y)){
		if (to.x > 0.0f){
			return EAST;
		}
		return WEST;

	} else {
		if (to.y > 0.0f){
			return SOUTH;
		}
		return NORTH;
	}

	return NUM_DIRECTIONS;
}

bool FgtNavArea::IsOverlappingArea( FgtNavArea *area ){
	if ( area->m_nwCorner.x < m_seCorner.x && area->m_seCorner.x > m_nwCorner.x && area->m_nwCorner.y < m_seCorner.y && area->m_seCorner.y > m_nwCorner.y ){
		return true;
	}
	return false;
}

void FgtNavArea::MarkAsBlocked( int teamID, CBaseEntity *blocker, bool bGenerateEvent ){
	if ( blocker && (strcmp( blocker->GetClassname(), "func_nav_blocker" ) == 0) ){
		m_attributeFlags |= NAV_MESH_NAV_BLOCKER;
	}

	bool wasBlocked = false;
	if ( teamID == TEAM_ANY ){
		for ( int i=0; i<MAX_NAV_TEAMS; ++i ){
			wasBlocked |= m_isBlocked[ i ];
			m_isBlocked[ i ] = true;
		}
	} else {
		int teamIdx = teamID % MAX_NAV_TEAMS;
		wasBlocked |= m_isBlocked[ teamIdx ];
		m_isBlocked[ teamIdx ] = true;
	}

	if ( !wasBlocked ){
		if ( bGenerateEvent ){
			IGameEvent * event = gameeventmanager->CreateEvent( "nav_blocked" );
			if ( event ){
				event->SetInt( "area", GetID() );
				event->SetInt( "blocked", 1 );
				gameeventmanager->FireEvent( event );
			}
		}
		g_NavMesh->OnAreaBlocked( this );
	}
}

void FgtNavArea::UnblockArea( int teamID ){
	bool wasBlocked = IsBlocked( teamID );

	if ( teamID == TEAM_ANY ){
		for ( int i=0; i<MAX_NAV_TEAMS; ++i ){
			m_isBlocked[ i ] = false;
		}
	} else {
		int teamIdx = teamID % MAX_NAV_TEAMS;
		m_isBlocked[ teamIdx ] = false;
	}

	if ( wasBlocked ){
		IGameEvent * event = gameeventmanager->CreateEvent( "nav_blocked" );
		if ( event ){
			event->SetInt( "area", GetID() );
			event->SetInt( "blocked", false );
			gameeventmanager->FireEvent( event );
		}
		g_NavMesh->OnAreaUnblocked( this );
	}
}