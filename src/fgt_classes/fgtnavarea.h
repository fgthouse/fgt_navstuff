#ifndef FgtNavArea_H
#define FgtNavArea_H

#include <nav_area.h>
#ifdef LINUX
	#undef min
	#undef max // Need to undefine valve's min/max definitions for vectors to cooperate on linux
#endif
#include <vector>

class FgtNavArea : public CNavArea {
	public:
		unsigned int& GetNearNavSearchMarker(); // This is for use in fgtnavmesh.h:GetNearestNavArea. It passes m_nearNavSearchMarker by reference.

		float GetZ( float x, float y );
		float GetZ( Vector vec );

		void GetClosestPointOnArea( const Vector * RESTRICT pPos, Vector *close );
		void GetClosestPointOnArea( const Vector &pos, Vector *close ) { return GetClosestPointOnArea( &pos, close ); }

		bool IsOverlapping( const Vector &pos, float tolerance = 0.0f );
		CNavArea* GetRandomAdjacentArea( NavDirType dir );
		void CollectAdjacentAreas( std::vector< CNavArea* > *adjVector );
		bool IsConnected( const CNavArea *area, NavDirType dir );
		float ComputeGroundHeightChange( FgtNavArea *area );
		NavDirType ComputeDirection( Vector *point );
		bool IsOverlappingArea( FgtNavArea *area );
		void MarkAsBlocked( int teamID, CBaseEntity *blocker, bool bGenerateEvent = true );
		void UnblockArea( int teamID = TEAM_ANY );
		//void UpdateBlocked( bool force, int teamID );
};

#endif
