// https://wiki.alliedmods.net/Signature_scanning

/* This was in the main.cpp. Just saving it for if I want to sigscan later
CSigScan::sigscan_dllfunc = *((void**)g_CNavMesh);
bool success = CSigScan::GetDllMemInfo();
CNavMesh_GetNearest_Sig.Init( (unsigned char*)GETNEARESTNAV_SIG, GETNEARESTNAV_SIGMASK, sizeof( GETNEARESTNAV_SIG ) );
if( CNavMesh_GetNearest_Sig.is_set ){
	Msg( "GetNearest: is_set 0x%x\n", CNavMesh_GetNearest_Sig.sig_addr );
} else {
	Warning( "Failed to sigscan CNavMesh::GetNearestNavArea!\n" );
}*/

#include <stdio.h>
 
#ifdef WIN32
    #define WIN32_LEAN_AND_MEAN
    #include <windows.h>
	#include <string.h>
#else
    #include <dlfcn.h>
    #include <sys/types.h>
    #include <sys/stat.h> 
#endif

#include "sigscan.h"
#include "eiface.h"
 
/* //////////////////////////////////////
    CSigScan Class
    ////////////////////////////////////// */
unsigned char* CSigScan::base_addr;
size_t CSigScan::base_len;
//void *(*CSigScan::sigscan_dllfunc)(const char *pName, int *pReturnCode);
void *CSigScan::sigscan_dllfunc;
 
/* Initialize the Signature Object */
void CSigScan::Init(unsigned char *sig, char *mask, size_t len) {
	is_set = 0;

	sig_len = len;
	sig_str = new unsigned char[ sig_len ];
	strcpy( (char*)sig_str, (char*)sig );
	
	sig_mask = new char[ sig_len ];
	strcpy( sig_mask, mask );

	if( !base_addr ){
		Warning( "CSigScan::Init Failed to get base_addr!\n" );
		return; // GetDllMemInfo() Failed
	}
	if( (sig_addr = FindSignature()) == NULL ){
		Warning( "CSigScan::Init failed to FindSignature!\n" );
		return; // FindSignature() Failed
	}

	is_set = 1;
}
 
/* Destructor frees sig-string allocated memory */
CSigScan::~CSigScan(void) {
	//Msg( "CSigScan::Deconstructor called\n" );
	delete[] sig_str;
	delete[] sig_mask;
}
 
/* Get base address of the server module (base_addr) and get its ending offset (base_len) */
bool CSigScan::GetDllMemInfo(void) {
	void *pAddr = (void*)sigscan_dllfunc;
	//Msg( "CSigScan::GetDllMemInfo pAddr: 0x%x\n", pAddr );
	base_addr = 0;
	base_len = 0;

#ifdef WIN32
	MEMORY_BASIC_INFORMATION mem;

	if(!pAddr){
		Warning( "CSigScan::GetDllMemInfo failed [!pAddr]\n" );
		return false; // GetDllMemInfo failed!pAddr
	}

	if(!VirtualQuery(pAddr, &mem, sizeof(mem))){
		Warning( "CSigScan::GetDllMemInfo failed [!VirtualQuery]\n" );
		return false;
	}

	base_addr = (unsigned char*)mem.AllocationBase;

	IMAGE_DOS_HEADER *dos = (IMAGE_DOS_HEADER*)mem.AllocationBase;
	IMAGE_NT_HEADERS *pe = (IMAGE_NT_HEADERS*)((unsigned long)dos+(unsigned long)dos->e_lfanew);

	if(pe->Signature != IMAGE_NT_SIGNATURE) {
		base_addr = 0;
		Warning( "CSigScan::GetDllMemInfo failed [pe->Signature != IMAGE_NT_SIGNATURE]\n" );
		return false; // GetDllMemInfo failedpe points to a bad location
	}

	base_len = (size_t)pe->OptionalHeader.SizeOfImage;
#else

	Dl_info info;
	struct stat buf;

	if(!dladdr(pAddr, &info))
		return false;

	if(!info.dli_fbase || !info.dli_fname)
		return false;

	if(stat(info.dli_fname, &buf) != 0)
		return false;

	base_addr = (unsigned char*)info.dli_fbase;
	base_len = buf.st_size;
#endif

	//Msg( "CSigScan::GetDllMemInfo page start: 0x%x \t end: 0x%x [Size: %ul]\n", base_addr, base_addr + base_len, base_len );
	return true;
}
 
/* Scan for the signature in memory then return the starting position's address */
void* CSigScan::FindSignature() {
	unsigned char *pBasePtr = base_addr;
	unsigned char *pEndPtr = base_addr + base_len;
	unsigned int i;
	//Msg( "Scanning from 0x%x to 0x%x\n", pBasePtr, pEndPtr );
	while( pBasePtr < pEndPtr ){
		for( i = 0; i < sig_len; i++ ){
			if( (sig_mask[i] != '?') && (sig_str[i] & 0xFF) != pBasePtr[i] ){
				break;
			}
		}
		if( i == (sig_len - 1) ){ // Don't forget the null terminator
			//Msg( "Full Match pBasePtr: 0x%x\n", pBasePtr );
			return (void*)pBasePtr;
		}
		pBasePtr += sizeof(unsigned char);
	}
	return NULL;
}

/* Set the static base_addr and base_len variables then initialize all Signature Objects */
void InitSigs(void) {
	CSigScan::GetDllMemInfo();
}