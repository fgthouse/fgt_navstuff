#include <stdio.h>

#include "GarrysMod/Lua/Interface.h"
#include "plugincallbacks.h"
#include "fgt_classes/fgtnavmesh.h"
#include "allied_sigscan/sigscan.h"

#include <stdio.h>
//#include <stdarg.h> // va_args
#include <map>
#include "interface.h"
#include "engine/iserverplugin.h"
#include "eiface.h"
#include "igameevents.h"
#include "convar.h"
#include "Color.h"
#include "vstdlib/random.h"
#include "engine/IEngineTrace.h"
#include "tier2/tier2.h"
#include "game/server/pluginvariant.h"
#include "game/server/iplayerinfo.h"
#include "game/server/ientityinfo.h"
#include "game/server/igameinfo.h"

// Interfaces from the engine
IVEngineServer  *engine = NULL; // helper functions (messaging clients, loading content, making entities, running commands, etc)
IGameEventManager2 *gameeventmanager = NULL; // game events interface
IPlayerInfoManager *playerinfomanager = NULL; // game dll interface to interact with players
IEntityInfoManager *entityinfomanager = NULL; // game dll interface to interact with all entities (like IPlayerInfo)
IGameInfoManager *gameinfomanager = NULL; // game dll interface to get data from game rules directly
IBotManager *botmanager = NULL; // game dll interface to interact with bots
IServerPluginHelpers *helpers = NULL; // special 3rd party plugin helpers from the engine
IUniformRandomStream *randomStr = NULL;
IEngineTrace *enginetrace = NULL;
CGlobalVars *gpGlobals = NULL;
ICvar *g_ICvar = NULL;

extern FgtNavMesh *g_NavMesh;
extern lua_State *g_LuaState;
extern bool g_FgtNavstuffLoaded;
extern int CNavAreaM_RefID;
extern int PlayerMeta_RefID;
extern int EntityMeta_RefID;
int HookCall_CFunc_Ref = NULL;

class FgtNavMap {
public:
	CNavArea* closestNav;
	CNavArea* lastNav;
};

std::map< CBaseEntity*, FgtNavMap* > playerNavMap; // KV Map of players to their last CNavArea

FgtNavStuffPlugin g_FgtNavStuffPlugin;
EXPOSE_SINGLE_INTERFACE_GLOBALVAR( FgtNavStuffPlugin, IServerPluginCallbacks, INTERFACEVERSION_ISERVERPLUGINCALLBACKS, g_FgtNavStuffPlugin );

// Call this on plugin shutdown/unload/pause
void cleanup(){
	g_FgtNavstuffLoaded = false;
	playerNavMap.clear();
	if( g_LuaState && HookCall_CFunc_Ref ){
		lua_State *state = g_LuaState;
		LUA->ReferenceFree( HookCall_CFunc_Ref );
	}
	HookCall_CFunc_Ref = NULL;
}

// This will be PlayerEntered/ExitedNavArea( player, cnavarea, cnavarea )
void LuaHookCall_OnEnterExit( const char *eventName, CBaseEntity *player, CNavArea *current, CNavArea *previous ){
	if( !g_LuaState ){ return; }
	if( !player || !current ){ return; }
	if( !g_FgtNavstuffLoaded || !CNavAreaM_RefID ){ return; }
#ifndef WIN32
	if( !PlayerMeta_RefID ){ return; } // Linux uses the Player metatable.
	// The above checks are in case the lua module calls gmod_module_close and the references are freed.
#endif
	lua_State *state = g_LuaState;

	if( !HookCall_CFunc_Ref ){ // We don't have a reference to hook.Call
		LUA->PushSpecial( GarrysMod::Lua::SPECIAL_GLOB );
		LUA->GetField( -1, "hook" );
		LUA->GetField( -1, "Call" );
		HookCall_CFunc_Ref = LUA->ReferenceCreate(); // This pops Call off the stack
		LUA->Pop( 2 ); // _G, hook
	}

	LUA->ReferencePush( HookCall_CFunc_Ref );
	LUA->PushString( eventName );
	LUA->PushNil();

#ifdef WIN32 // Just use Entity( entIndex ) to get a CBaseHandle pointer to the entity. (fucking ntdll.dll protecting shit)
	LUA->PushSpecial( GarrysMod::Lua::SPECIAL_GLOB );
	LUA->GetField( -1, "Entity" );
	LUA->PushNumber( player->entindex() );

	LUA->Call( 1, 1 );
	GarrysMod::Lua::UserData *playerUd = (GarrysMod::Lua::UserData*)LUA->GetUserdata( -1 );
	CBaseHandle ply = *(CBaseHandle*)playerUd->data;
	if( ply.IsValid() ){ // The player is valid. Move the stack around to pop _G but keep our userdata on top.
		LUA->Insert( -2 ); // Move our player userdata below _G so we can pop _G
		LUA->Pop(); // _G
	} else {
		Warning( "LuaHookCall_OnEnterExit Invalid entity of entindex %i\n", player->entindex() );
		LUA->Pop( 2 ); // nil and _G
		return;
	}
#else
	GarrysMod::Lua::UserData *playerUd = (GarrysMod::Lua::UserData*)LUA->NewUserdata( sizeof( GarrysMod::Lua::UserData ) );
	playerUd->data = new CBaseHandle( player->GetRefEHandle() );
	playerUd->type = GarrysMod::Lua::Type::ENTITY;
	LUA->ReferencePush( PlayerMeta_RefID );
	LUA->SetMetaTable( -2 );
#endif

	GarrysMod::Lua::UserData *navUd = (GarrysMod::Lua::UserData*)LUA->NewUserdata( sizeof( GarrysMod::Lua::UserData ) );
	navUd->data = current;
	navUd->type = 37;
	LUA->ReferencePush( CNavAreaM_RefID );
	LUA->SetMetaTable( -2 );

	if( !previous ){
		LUA->PushNil();
	} else {
		GarrysMod::Lua::UserData *navUd = (GarrysMod::Lua::UserData*)LUA->NewUserdata( sizeof( GarrysMod::Lua::UserData ) );
		navUd->data = previous;
		navUd->type = 37;
		LUA->ReferencePush( CNavAreaM_RefID );
		LUA->SetMetaTable( -2 );
	}

	LUA->Call( 5, 0 );
}

FgtNavStuffPlugin::FgtNavStuffPlugin(){
	m_iClientCommandIndex = 0;
}
FgtNavStuffPlugin::~FgtNavStuffPlugin(){}

bool FgtNavStuffPlugin::Load(  CreateInterfaceFn interfaceFactory, CreateInterfaceFn gameServerFactory ){
	//Msg( "FgtNavStuffPlugin::Load\n" );
	ConnectTier1Libraries( &interfaceFactory, 1 );
	ConnectTier2Libraries( &interfaceFactory, 1 );

	/*entityinfomanager = (IEntityInfoManager *)gameServerFactory( INTERFACEVERSION_ENTITYINFOMANAGER, NULL );
	if ( !entityinfomanager ){
		Warning( "FgtNavStuffPlugin::Load Unable to load entityinfomanager, ignoring\n" ); // this isn't fatal, we just won't be able to access entity data
	}*/

	playerinfomanager = (IPlayerInfoManager *)gameServerFactory( INTERFACEVERSION_PLAYERINFOMANAGER, NULL );
	if ( !playerinfomanager ){
		Warning( "FgtNavStuffPlugin::Load Unable to load playerinfomanager, ignoring\n" ); // this isn't fatal, we just won't be able to access specific player data
	}

	/*botmanager = (IBotManager *)gameServerFactory( INTERFACEVERSION_PLAYERBOTMANAGER, NULL );
	if ( !botmanager ){
		Warning( "FgtNavStuffPlugin::Load Unable to load botcontroller, ignoring\n" ); // this isn't fatal, we just won't be able to access specific bot functions
	}*/
	/*gameinfomanager = (IGameInfoManager *)gameServerFactory( INTERFACEVERSION_GAMEINFOMANAGER, NULL );
	if (!gameinfomanager){
		Warning( "FgtNavStuffPlugin::Load Unable to load gameinfomanager, ignoring\n" );
	}*/

	enginetrace = (IEngineTrace *)interfaceFactory( INTERFACEVERSION_ENGINETRACE_SERVER, NULL );
	g_ICvar = (ICvar*)interfaceFactory( CVAR_INTERFACE_VERSION, NULL );
	engine = (IVEngineServer*)interfaceFactory( INTERFACEVERSION_VENGINESERVER_VERSION_21, NULL );
	gameeventmanager = (IGameEventManager2 *)interfaceFactory( INTERFACEVERSION_GAMEEVENTSMANAGER2, NULL );
	//helpers = (IServerPluginHelpers*)interfaceFactory( INTERFACEVERSION_ISERVERPLUGINHELPERS, NULL);
	//randomStr = (IUniformRandomStream *)interfaceFactory( VENGINE_SERVER_RANDOM_INTERFACE_VERSION, NULL );

	if ( playerinfomanager ){
		gpGlobals = playerinfomanager->GetGlobalVars();
	}

	//MathLib_Init( 2.2f, 2.2f, 0.0f, 2.0f );
	//ConVar_Register( 0 );
	return true;
}

void FgtNavStuffPlugin::Unload( void ){
	//Msg( "FgtNavStuffPlugin::Unload\n" );
	//gameeventmanager->RemoveListener( this ); // make sure we are unloaded from the event system

	cleanup();

	//ConVar_Unregister( );
	DisconnectTier2Libraries( );
	DisconnectTier1Libraries( );
}

const char* FgtNavStuffPlugin::GetPluginDescription(){
	return "fgt_navstuff (mcd1992)";
}

void FgtNavStuffPlugin::LevelInit( char const *pMapName ){
	//Msg( "FgtNavStuffPlugin::LevelInit\n" );
	//gameeventmanager->AddListener( this, "player_say", true );
	cleanup();
}

void FgtNavStuffPlugin::GameFrame( bool simulating ){
	if( !g_LuaState || !g_FgtNavstuffLoaded ){ return; }
	if( g_NavMesh->IsGenerating() || !g_NavMesh->m_areaCount ){ return; }

	for( int i = 1; i < gpGlobals->maxClients; i++ ){
		edict_t *ent = engine->PEntityOfEntIndex( i );
		IServerUnknown *unk = ent->GetUnknown();
		if( unk ){
			CBaseEntity *ply = unk->GetBaseEntity();
			Vector plyPos = ply->GetCollideable()->GetCollisionOrigin();
			//Msg( "%f, %f, %f\n", plyPos.x, plyPos.y, plyPos.z );
			CNavArea *nav = g_NavMesh->GetNavArea( plyPos ); // Do a quick check for a navarea directly under player

			if( !nav ){
				nav = g_NavMesh->GetNearestNavArea( plyPos, false, 240.0f, true, true ); // Get closest navarea in los
			}
			if( !nav ){
				nav = g_NavMesh->GetNavArea( plyPos, 10000.0f ); // Look for any navareas directly below us ignoring Z
			}
			if( !nav ){ // The player is either oob or the map has no navmesh nearby. Just find one anywhere.
				nav = g_NavMesh->GetNearestNavArea( plyPos, false, 10000.0f, false, true ); // Player isn't on/near a navarea. Pick closest.
			}
			if( nav ){
				FgtNavMap *last = playerNavMap[ ply ];
				if( last ){
					if( last->lastNav != nav ){
						LuaHookCall_OnEnterExit( "PlayerEnteredNavArea", ply, nav, last->lastNav );
						LuaHookCall_OnEnterExit( "PlayerExitedNavArea", ply, last->lastNav, nav );
						last->closestNav = nav;
						last->lastNav = nav;
					}

				} else { // Add the player initially
					playerNavMap[ ply ] = new FgtNavMap();
					playerNavMap[ ply ]->closestNav = nav;
					playerNavMap[ ply ]->lastNav = nav;
					LuaHookCall_OnEnterExit( "PlayerEnteredNavArea", ply, nav, NULL );
				}
			} else {
				FgtNavMap *last = playerNavMap[ ply ];
				if( last->lastNav ){
					LuaHookCall_OnEnterExit( "PlayerExitedNavArea", ply, last->lastNav, NULL ); // Player left the navigation mesh
					last->lastNav = NULL;
				}
			}
		}
	}
}

void FgtNavStuffPlugin::SetCommandClient( int index ){
	m_iClientCommandIndex = index;
}

void FgtNavStuffPlugin::FireGameEvent( IGameEvent *event ){
	const char *name = event->GetName();
	//Msg( "Received event: %s\n", name );
	//if( FStrEq( name, "player_say" ) ){}
}


void FgtNavStuffPlugin::ClientDisconnect( edict_t *pEntity ){
	//CBasePlayer *ply = ((CBasePlayer*)CBaseEntity::Instance( pEntity )); // Get player object from edict
	//CBaseEntity *ent = pEntity->GetUnknown()->GetBaseEntity(); // Get entity object from edict
	IServerUnknown *unk = pEntity->GetUnknown();
	if( unk ){
		CBaseEntity *ply = unk->GetBaseEntity();
		if( playerNavMap[ ply ] ){
			playerNavMap.erase( ply );
		}
	}
}

void FgtNavStuffPlugin::ClientActive( edict_t *pEntity ){
}

PLUGIN_RESULT FgtNavStuffPlugin::NetworkIDValidated( const char *pszUserName, const char *pszNetworkID ){
	return PLUGIN_CONTINUE;
}
PLUGIN_RESULT FgtNavStuffPlugin::ClientCommand( edict_t *pEntity, const CCommand &args ){
	return PLUGIN_CONTINUE;
}
PLUGIN_RESULT FgtNavStuffPlugin::ClientConnect( bool *bAllowConnect, edict_t *pEntity, const char *pszName, const char *pszAddress, char *reject, int maxrejectlen ){
	return PLUGIN_CONTINUE;
}
void FgtNavStuffPlugin::Pause(){
	cleanup();
}
void FgtNavStuffPlugin::LevelShutdown(){
	cleanup();
}
void FgtNavStuffPlugin::UnPause(){}
void FgtNavStuffPlugin::OnEdictAllocated( edict_t *edict ){}
void FgtNavStuffPlugin::OnEdictFreed( const edict_t *edict ){}
void FgtNavStuffPlugin::OnQueryCvarValueFinished( QueryCvarCookie_t iCookie, edict_t *pPlayerEntity, EQueryCvarValueStatus eStatus, const char *pCvarName, const char *pCvarValue ){}
void FgtNavStuffPlugin::ClientSettingsChanged( edict_t *pEdict ){}
void FgtNavStuffPlugin::ClientPutInServer( edict_t *pEntity, char const *playerName ){}
void FgtNavStuffPlugin::ServerActivate( edict_t *pEdictList, int edictCount, int clientMax ){}
